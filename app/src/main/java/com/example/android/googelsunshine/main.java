package com.example.android.googelsunshine;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.view.accessibility.AccessibilityEventSource;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class main extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        private ArrayAdapter<String> mFocastAdapter;
        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            String [] forecast = {
                    "Today          - sunny     - 88/63",
                    "Tomorrow       - foggy      -70/40",
                    "Weds           - cloudy     - 7263",
                    "Thus           - Asteroid   - 75/65",
                    "Friday         - Heavy rain  - 65/56",
                    "Sat            - help         -65/51",
                    "sun            -sunny          -80/68"
            };

            List<String> weekForecast = new ArrayList<String>( Arrays.asList(forecast) );


            //initialize adapter
             mFocastAdapter = new ArrayAdapter<String>(
                    //the parent context
                    getActivity(),

                    // id of list item layout
                    R.layout.list_item_forecast,

                    //id of text view to populate
                    R.id.list_item_forecast_textview,

                    //forecast data
                    weekForecast
            );


            ListView listView = (ListView) rootView.findViewById(R.id.listview_forecast);
            listView.setAdapter(mFocastAdapter);
            return rootView;
        }
    }
}
